package Model;

import java.util.List;

/**
 * Created by Андрей on 25.06.2017.
 */
public class Developer {

    private int id;
    private int salary;
    private String name;
    private List<Skill> skills = null;


    public Developer() {
    }

    public Developer(String name,int salary) {
        this.salary = salary;
        this.name = name;
    }

    public Developer(int id, int salary, String name, List<Skill> sklls) {
        this.id = id;
        this.salary = salary;
        this.name = name;
        this.skills = sklls;
    }

    public Developer(int salary, String name, List<Skill> sklls) {
        this.id = 0;
        this.salary = salary;
        this.name = name;
        this.skills = sklls;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSklls(List<Skill> sklls) {
        this.skills = sklls;
    }
}
